import React from "react";
import { Ionicons } from "@expo/vector-icons";
import Icon from 'react-native-vector-icons/Ionicons';

import { createAppContainer } from "react-navigation";
import { createMaterialBottomTabNavigator } from
    "react-navigation-material-bottom-tabs";

import Home from "./screens/Home";
import UserScreen from "./screens/UserScreen";
import Settings from "./screens/Settings";

const TabNavigator = createMaterialBottomTabNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarLabel: "Trang Chủ",
        tabBarIcon: (tabInfo) => (
          <Icon
            name="home"
            size={tabInfo.focused ? 26 : 20}
            color={tabInfo.tintColor}
          />
        ),
      },
    },
    User: {
      screen: UserScreen,
      navigationOptions: {
        tabBarLabel: "Cá nhân",
        tabBarIcon: (tabInfo) => (
          <Icon
            name="shield"
            size={tabInfo.focused ? 26 : 20}
            color={tabInfo.tintColor}
          />
        ),
      },
    },
    Setting: {
      screen: Settings,
      navigationOptions: {
        tabBarLabel: "Cài đặt",
        tabBarIcon: (tabInfo) => (
          <Icon
            name="md-settings-outline"
            size={tabInfo.focused ? 26 : 20}
            color={tabInfo.tintColor}
          />
        ),
      },
    },
  },
  {
    initialRouteName: "Home",
    barStyle: { backgroundColor: "#006600" },
  }
);

const Navigator = createAppContainer(TabNavigator);

export default function App() {
  return (
    <Navigator>
      <Home />
    </Navigator>
  );
}
