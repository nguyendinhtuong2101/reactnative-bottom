import React from "react";
import { Text, View } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';

const Home = () => {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text style={{ color: "#006600", fontSize: 40 }}>Home Screen!</Text>
      <Icon name="rocket" size={30} color="#900" />
    </View>
  );
};

export default Home;
